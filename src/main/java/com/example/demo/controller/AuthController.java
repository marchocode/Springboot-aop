package com.example.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Copyright (C), 2015-2019
 * FileName: AuthController
 * Author:   MRC
 * Date:     2019/11/9 9:43
 * Description:
 * History:
 */
@RestController
@RequestMapping("/")
public class AuthController {

    private Logger logger = LoggerFactory.getLogger(AuthController.class);

    @GetMapping("login")
    public String login(String user,String pass) throws Exception {

        logger.info("login--->user",user);
        logger.info("login--->pass",pass);
//        logger.info("将要抛出异常");
//        int a = 1/0;
        return "success";
    }

}